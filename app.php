<?php

namespace App;

class App
{
    protected   $handler,
                $input,
                $name,
                $mark;
    
    public function setHandler ()
    {
        $this->handler = fopen("php://stdin", 'r');
        return $this;
    }
    
    public function getHandler()
    {
        return $this->setHandler()->handler;
    }
    
    public function setInput ($input)
    {
        $this->input = trim($input);
        return $this;
    }
    
    public function getInput()
    {
        return $this->input;
    }
    
    public function setName ($name)
    {
        $this->name = $name;
        return $this;
    }
    
    public function getName ()
    {
        return $this->name;
    }
    
    public function setMark ($mark)
    {
        $this->mark = $mark;
        return $this;
    }
    
    public function getMark ()
    {
        return $this->mark;
    }
    
    public function askme ()
    {
        $this->setInput(
        fgets(
            $this->getHandler()
        ));
    }
    
    public function answer ()
    {
        return $this->getInput();
    }
    
    public function askName ()
    {
        $this->askme();
        $this->setName($this->getInput());
        return $this;
    }
    
        public function askMark ()
    {
        $this->askme();
        $this->setMark($this->getInput());
        return $this;
    }
    
}

?>